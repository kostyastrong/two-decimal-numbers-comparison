A user should enter 2 decimal numbers, both in the particular style:

1. only one sign: either + or -
2. whole part
3. only one comma
4. decimal part

for example allowed: +1,22403 or -3,0 or 58729,004500000
and not: -+1,30,3 or ++34,423

A user gets the correct answer only for correct input.
