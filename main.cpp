#include <iostream>
#include <string>

class StringMod{
 public:
  explicit StringMod(std::string data);
  friend bool operator<=(const StringMod& l, const StringMod& r);
  friend bool operator>=(const StringMod& l, const StringMod& r);

 private:
  std::string data_;
  bool isNegative_ = false;
};

void ReadDouble(std::string& cinDouble) {
  char firstChar;
  std::cin >> firstChar;
  if (firstChar != '+') {
    cinDouble += firstChar;
  }
  std::string secondPart;
  std::cin >> secondPart;
  while (secondPart.back() == '0') {
    secondPart.pop_back();
  }
  if (secondPart.back() == ',') secondPart.push_back('0');
  cinDouble += secondPart;
}

bool operator<=(const StringMod& l, const StringMod& r) {
  if (l.isNegative_ == r.isNegative_) {
    size_t ind = 0;
    size_t lenMin = std::min(l.data_.size(), r.data_.size());
    while (ind < lenMin && l.data_[ind] == r.data_[ind]) {
      ++ind;
    }
    bool less;
    if (ind == lenMin) {
      if (l.data_.size() == r.data_.size()) {
        less = true;
      } else {
        less = l.isNegative_ ^ (l.data_.size() < r.data_.size());
      }
    } else {
      less = l.isNegative_ ^ (l.data_[ind] < r.data_[ind]);
    }
    return less;
  }
  return static_cast<int>(l.isNegative_) > static_cast<int>(r.isNegative_);
}

StringMod::StringMod(std::string data)  {
  data_ = std::move(data);
  isNegative_ = data_[0] == '-';
}

bool operator>=(const StringMod& l, const StringMod& r) {
  return r <= l;
}

class Solver {
 public:
  void operator()() {
    std::string number_1;
    std::string number_2;
    ReadDouble(number_1);
    ReadDouble(number_2);
    StringMod first = StringMod(number_1);
    StringMod second = StringMod(number_2);
    bool less = first <= second;
    bool more = first >= second;
    if (less && more) {
      std::cout << "equal";
    } else if (less) {
      std::cout << "less";
    } else {
      std::cout << "more";
    }
  }
};

int main() {
  Solver solver;
  solver();
}